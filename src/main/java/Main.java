import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

/**
 * Klasse, welche die Funktionalitaet implementiert, um diverse Abfragen auf einer Datenbank laufen zu lassen und
 * die Resultate dann in ein txt file zu schreiben.
 * 
 * @author Giancarlo Bergamin
 * @author Yvo Broennimann
 * @author Melanie Calame
 * @author Anna-Lena Klaus
 * @version 1.0
 *
 */
public class Main {

	
	static Scanner sc;
	
	/**
	 * In dieser main Methode wird eine Connection zum datenbankserver erstellt und dann kann eingegeben werden, welche
	 * Abfrage ausgefuehrt werden soll. Nach der ausfuehrung des querys wird gefragt ob ein weiteres query laufen gelassen
	 * werden soll. Dies so lange bis man keine weiteren Abfragen mehr erstellen will.
	 * @param args
	 *            none required
	 */
	public static void main(String[] args) {
		
		//Loading properties such as the url, the username and the password required to connect to the database from the config.properties file
		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e1) {
			System.out.println(e1);
			e1.printStackTrace();
		}
		String connectString = properties.getProperty("jdbc.url");
		String user = properties.getProperty("jdbc.username");
		String password = properties.getProperty("jdbc.password");
		
	    String newLine = System.getProperty("line.separator");
	    
		sc = new Scanner(System.in);
		int querynr;
	    boolean again = true;
	    
		while (again){
			System.out.println("Waehlen sie die auszufuehrende Abfrage aus (Nummer): \n" + newLine 
					+ "1:  Uebersicht, welche Gamer sich in welchen Chat-Raeumen unterhalten." + newLine
					+ "2:  Uebersicht, welche Gamer sich in einem bestimmten Chat-Raum unterhalten." + newLine
					+ "3:  Uebersicht in welchen Chat-Raeumen sich ein bestimmter Gamer unterhaelt." + newLine
					+ "4:  Durchschnittliche Laenge der Messages pro Chat-Room." + newLine
					+ "5:  Anzahl der Chat-Sessions pro Gamer." + newLine
					+ "6:  Anzahl der Chat-Sessions eines spezifischen Gamers." + newLine
					+ "7:  Groesse der Gruppen in den Chat-Raeumen." + newLine
					+ "8:  Groesse der Gruppe in einem bestimmten Chatroom." + newLine
					+ "9:  Gamer ohne Chats." + newLine
					+ "10: Durchschnittliche Anzahl Chats pro Session, sowie Maximale Zahl und Minimale Zahl." + newLine
					+ "11: Liste der Chat-Sessions mit Datum/Zeit, Name des Chat-Raums, Liste der Chat-Partner, Anzahl getauschter Nachrichten. \n" + newLine
					+ "Eingabe: \n");
			
						
			while (!sc.hasNextInt()) {
				   System.out.println("Bitte eine Nummer eingeben.");
				   sc.nextLine();
				}
			
			// Ueberprueft die Eingabe der Abfragenummer, welche zwischen 1 und 11 liegen muss (bei den momentanen Abfragen).
			boolean mal = true;
			while (mal){
				querynr = sc.nextInt();
	        	if (0 < querynr && querynr < 12){
	        		mal = false;
	        	
			
				// Ein switch, welcher die Methode fuer das ausgewaehlte query aufruft.
				switch (querynr) {
			        case 1:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query1(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 2:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query2(con, sc);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 3:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query3(con, sc);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 4:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query4(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 5:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query5(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 6:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query6(con, sc);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 7:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query7(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 8:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query8(con, sc);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 9:  try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query9(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 10: try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query10(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
			        case 11: try {
									Connection con = DriverManager.getConnection(connectString, user, password);
									query11(con);
									con.close();
								 } catch (SQLException e) {
									e.printStackTrace();
								 }
			        		break;
					}
	        	}
	        	else {
	        		System.out.println("Bitte eine Zahl zwischen 1 und 11 eingeben (Abfragenummern)");
	        	}
	        }
			
	        System.out.println("Wollen sie eine weitere Abfrage durchfuehren? (y/n)");
	        
	        // Ueberprueft, ob die Eingabe zuslaessig ist (nur die Werte 'y' und 'n' sind erlaubt)
	        boolean nochmal = true;
	        
	        while (nochmal){
	        	String answer = sc.next();
	        	if (answer.equals("n") || answer.equals("y")){
	        		nochmal = false;
	        		if (answer.equals("n")) { 
	        			again = false;
	        			System.out.println("Auf wiedersehen.");
	        		}
	        	}
	        	else {
	        		System.out.println("Bitte mit 'y' bestaetigen oder mit 'n' ablehnen.");
	        	}
	        }
	        
	        
		}
		sc.close();
	

	}
	
	/**
	 * Fuehrt das query 1 aus: Uebersicht, welche Gamer sich in welchen Chat-Raeumen unterhalten. Und schreibt
	 * die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * @param con Die Datenbank Connection
	 */
	public static void query1(Connection con){
		
		try {
			String query = "SELECT gamer.name as Gamer, chatroom.name as Chatroom, message.date as Date, message.text as Message, length(message.text) as 'Length of message' "
						+ "FROM gamer "
						+ "JOIN gamer_has_chatroom ON gamer.name = gamer_has_chatroom.gamer_name "
						+ "JOIN chatroom ON chatroom.name = gamer_has_chatroom.chatroom_name "
						+ "JOIN message ON message.chatroom_name = chatroom.name";
					
			PreparedStatement stmt = con.prepareStatement(query);					
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
	
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage1_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", "Gamer", "Chatroom", "Datum", "Nachricht", "Laenge der Nachricht");
			    String line = String.format("%0172d", 0).replace("0","-");
			    String doubleline = String.format("%0172d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("Gamer"); 
			    	String chatroom = res.getString("Chatroom");
			    	Date date = res.getDate("Date");
			    	String message = res.getString("Message");
			    	int length = res.getInt("Length of message");
			    	//writer.println(gamer + "     "+ chatroom + "     " + date + "     " + message + "     " + length);
			    	output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", gamer, chatroom, date, message, length);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	/**
	 * Fuehrt das query 2 aus: Ubersicht, welche Gamer sich in einem bestimmten Chat-Raum unterhalten. Und schreibt
	 * die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 * @param sc Der Input scanner
	 */
	public static void query2(Connection con, Scanner sc){
		
		try {
			String query = "SELECT gamer.name as Gamer, chatroom.name as Chatroom , message.date as Date, message.text as Message, length(message.text) as 'Length of message' FROM gamer "
					+ "JOIN gamer_has_chatroom ON gamer.name = gamer_has_chatroom.gamer_name "
					+ "JOIN chatroom ON chatroom.name = gamer_has_chatroom.chatroom_name "
					+ "JOIN message ON message.chatroom_name = chatroom.name "
					+ "WHERE chatroom.name = ?";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			System.out.println("Bitte geben sie den Namen des Chatrooms ein: \n");
			String input = sc.next();
			
			stmt.setString(1, input);
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage2_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", "Gamer", "Chatroom", "Datum", "Nachricht", "Laenge der Nachricht");
			    String line = String.format("%0172d", 0).replace("0","-");
			    String doubleline = String.format("%0172d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("Gamer"); 
			    	String chatroom = res.getString("Chatroom");
			    	Date date = res.getDate("Date");
			    	String message = res.getString("Message");
			    	int length = res.getInt("Length of message");
			    	//writer.println(gamer + "     "+ chatroom + "     " + date + "     " + message + "     " + length);
			    	
			    	output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", gamer, chatroom, date, message, length);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Fuehrt das query 3 aus: Uebersicht in welchen Chat-Räumen sich ein bestimmter Gamer unterhaelt, 
	 * inklusive Angabe des Datums, der Message und der Message-Laenge. Und schreibt
	 * die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 * @param sc Der Input scanner
	 */
	public static void query3(Connection con, Scanner sc){
		
		try {
			String query = "SELECT gamer.name as Gamer, chatroom.name as Chatroom, message.date as Date, message.text as Message, LENGTH(message.text) as 'Length of Message' FROM gamer "
					+ "JOIN gamer_has_chatroom ON gamer.name = gamer_has_chatroom.gamer_name "
					+ "JOIN chatroom ON chatroom.name = gamer_has_chatroom.chatroom_name "
					+ "JOIN message ON message.chatroom_name = chatroom.name "
					+ "WHERE gamer.name = ?";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			System.out.println("Bitte geben sie den Namen des Gamers ein: ");
			String input = sc.nextLine();
			
			stmt.setString(1, input);
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage3_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", "Gamer", "Chatroom", "Datum", "Nachricht", "Laenge der Nachricht");
			    String line = String.format("%0172d", 0).replace("0","-");
			    String doubleline = String.format("%0172d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("Gamer"); 
			    	String chatroom = res.getString("Chatroom");
			    	Date date = res.getDate("Date");
			    	String message = res.getString("Message");
			    	int length = res.getInt("Length of message");
			    	//writer.println(gamer + "     "+ chatroom + "     " + date + "     " + message + "     " + length);
			    	
			    	output = String.format("|%-25s|%-40s|%-15s|%-80s|%-6s|", gamer, chatroom, date, message, length);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 4 aus: Durchschnittliche Laenge der Messages pro Chat-Room. Und schreibt
	 * die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query4(Connection con){
		
		try {
			String query = "SELECT chatroom.name as Chatroom, AVG(LENGTH(text)) as 'Average length of messages' FROM message "
					+ "JOIN chatroom ON message.chatroom_name = chatroom.name "
					+ "GROUP BY chatroom.name";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage4_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|%-30s|", "Chatroom", "Durchschnittliche Laenge der Nachricht");
			    String line = String.format("%073d", 0).replace("0","-");
			    String doubleline = String.format("%073d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {

			    	String chatroom = res.getString("Chatroom");
			    	int length = res.getInt("Average length of messages");
			    	
			    	output = String.format("|%-40s|%-30s|", chatroom, length);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Fuehrt das query 5 aus: Anzahl der Chat-Sessions pro Gamer --> Chat-Sessions = 1 Tag in 1 Chat-Room. 
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query5(Connection con){
		
		try {
			String query = "SELECT message.gamer_name 'gamer', COUNT(Date(message.date)) as 'Number of Sessions' FROM message "
					+ "GROUP BY gamer";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage5_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|%-25s|", "Gamer","Anzahl Sessions");
			    String line = String.format("%068d", 0).replace("0","-");
			    String doubleline = String.format("%068d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("gamer"); 
			    	int number = res.getInt("Number of Sessions");
			    	
			    	output = String.format("|%-40s|%-25s|", gamer, number);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 6 aus: Anzahl der Chat-Sessions eines spezifischen Gamers --> Chat-Sessions = 1 Tag in 1 Chat-Room. 
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 * @param sc Der input scanner
	 */
	public static void query6(Connection con, Scanner sc){
		
		try {
			String query = "SELECT message.gamer_name 'gamer', COUNT(Date(message.date)) as 'Number of Sessions' FROM message "
					+ "WHERE message.gamer_name = ? "
					+ "GROUP BY gamer";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			System.out.println("Bitte geben sie den Namen des Gamers ein: ");
			String input = sc.next();
			stmt.setString(1, input);
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage6_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|%-25s|", "Gamer", "Anzahl Sessions");
			    String line = String.format("%068d", 0).replace("0","-");
			    String doubleline = String.format("%068d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("gamer"); 
			    	int number = res.getInt("Number of Sessions");
			    	
			    	output = String.format("|%-40s|%-25s|", gamer, number);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 7 aus: Groesse der Gruppen in den Chat-Raeumen.
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query7(Connection con){
		
		try {
			String query = "SELECT chatroom.name as 'Chatroom', COUNT(gamer_has_chatroom.gamer_name) as 'Anzahl Gamer im Chatroom' FROM gamer_has_chatroom "
					+ "JOIN chatroom ON chatroom.name = gamer_has_chatroom.chatroom_name "
					+ "GROUP BY chatroom.name";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage7_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    
			    String output = String.format("|%-40s|%-30s|", "Chatroom", "Anzahl Gamer im Chatroom");
			    String line = String.format("%073d", 0).replace("0","-");
			    String doubleline = String.format("%073d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String chatroom = res.getString("Chatroom"); 
			    	int number = res.getInt("Anzahl Gamer im Chatroom");
			    	
			    	output = String.format("|%-40s|%-30s|", chatroom, number);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 8 aus: Groesse der Gruppe in einem bestimmten Chatroom.
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 * @param sc Der input scanner
	 */
	public static void query8(Connection con, Scanner sc){
		
		try {
			String query = "SELECT chatroom.name as 'Chatroom', COUNT(gamer_has_chatroom.gamer_name) as 'Anzahl Gamer im Chatroom' FROM gamer_has_chatroom "
					+ "JOIN chatroom ON chatroom.name = gamer_has_chatroom.chatroom_name "
					+ "WHERE chatroom.name = ?"
					+ "GROUP BY chatroom.name";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			System.out.println("Bitte geben sie den Namen des Chatrooms ein: ");
			String input = sc.next();
			
			stmt.setString(1, input);
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage8_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|%-30s|", "Chatroom", "Anzahl Gamer im Chatroom");
			    String line = String.format("%073d", 0).replace("0","-");
			    String doubleline = String.format("%073d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String chatroom = res.getString("Chatroom"); 
			    	int number = res.getInt("Anzahl Gamer im Chatroom");
			    	
			    	output = String.format("|%-40s|%-30s|", chatroom, number);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 9 aus: Gamer ohne Chats.
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query9(Connection con){
		
		try {
			String query = "SELECT gamer.name as 'Gamer with no chats' FROM gamer "
					+ "LEFT OUTER JOIN gamer_has_chatroom ON gamer.name = gamer_has_chatroom.gamer_name "
					+ "WHERE chatroom_name IS NULL";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage9_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|", "Gamer");
			    String line = String.format("%042d", 0).replace("0","-");
			    String doubleline = String.format("%042d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamer = res.getString("Gamer with no chats"); 
			    	
			    	output = String.format("|%-40s|", gamer);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 10 aus: Durchschnittliche Anzahl Chats pro Session, sowie Maximale Zahl und Minimale Zahl.
	 * Chats = Messages, Session wird hier mit 1 Tag in 1 Chat-Room gleichgesetzt..
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query10(Connection con){
		
		try {
			String query = "SELECT AVG(nrOfMsg) 'Average number of Messages in chatrooms', MAX(nrOfMsg) as 'Max. number of messages in chatroom', Min(nrOfMsg) as 'Min. number of messages in chatroom' from "
					+ "(SELECT COUNT(message.id) as nrOfMsg FROM message "
					+ "RIGHT JOIN chatroom ON chatroom.name = message.chatroom_name "
					+ "GROUP BY chatroom_name, DATE(message.date)) stats";
					
			PreparedStatement stmt = con.prepareStatement(query);
			
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
			
			try{
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage10_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-50s|%-50s|%-50s|", "Durchschnittliche Anzahl Nachrichten in Chatrooms", "Max. Anzahl Nachrichten in Chatrooms", "Min. Anzahl Nachrichten in Chatrooms");
			    String line = String.format("%0153d", 0).replace("0","-");
			    String doubleline = String.format("%0153d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	float average = res.getFloat("Average number of Messages in chatrooms"); 
			    	int min = res.getInt("Max. number of messages in chatroom");
			    	int max = res.getInt("Min. number of messages in chatroom");
			    	
			    	output = String.format("|%-50s|%-50s|%-50s|", average, min, max);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fuehrt das query 11 aus: Liste der Chat-Sessions mit Datum/Zeit, Name des Chat-Raums, Liste der Chat-Partner, Anzahl getauschter Nachrichten.
	 * Session = 1 Tag in 1 Chat-Raum.
	 * Und schreibt die Ergebnisse in ein Textfile mit der Querybezeichnung und dem heutigen Datum.
	 * 
	 * @param con Die Datenbank connection
	 */
	public static void query11(Connection con){
		
		try {
			String query = "create or replace view a as "
					+ "Select chatroom.name as Chatroom, Date(message.date) as Date, count(message.id) as MsgCount, gamer.name as Gamer from chatroom " 
					+ "join message on chatroom.name = message.chatroom_name "
					+ "join gamer on message.gamer_name = gamer.name "
					+ "Group by chatroom.name, Date(message.date), gamer.name";
			
			String query2 =  "select a.Chatroom, a.Date, a.MsgCount as 'Number of messages', group_concat(a.Gamer) as 'Gamers' from a "
					+ "Group by Chatroom, Date, MsgCount";
					
			PreparedStatement stmt = con.prepareStatement(query2); // Statement wird an DB geschickt und von der DB geprueft. Es wird vorkompiliert, falls es richtig ist
			PreparedStatement createview = con.prepareStatement(query);					
			createview.execute();
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus
	
			try{
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date rundate = new Date();
				String formatdate = dateFormat.format(rundate);
				String filename = "Abfrage11_" + formatdate;
				
			    PrintWriter writer = new PrintWriter(filename, "UTF-8");
		
			    String output = String.format("|%-40s|%-40s|%-20s|%-40s|", "Chatroom", "Datum", "Anzahl Nachrichten", "Gamers");
			    String line = String.format("%0145d", 0).replace("0","-");
			    String doubleline = String.format("%0145d", 0).replace("0","="); 
			    
			    writer.println(doubleline);
			    writer.println(output);
			    writer.println(doubleline);
			    
			    while (res.next()) {
			    	String gamers = res.getString("Gamers"); 
			    	String chatroom = res.getString("Chatroom");
			    	Date date = res.getDate("Date");
			    	int number = res.getInt("Number of messages");
			    	
			    	output = String.format("|%-40s|%-40s|%-20s|%-40s|", chatroom, date, number, gamers);
			    	writer.println(output);
			    	writer.println(line);
			    }
			    writer.close();
			}
			 catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	
}
